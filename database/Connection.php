<?php

/**
 * Created by: Stephan Hoeksema 2022
 * wfflix-cd
 * option+command+L voor windows = control+alt+L voor uitlijnen code
 */
class Connection
{
    public static function make()
    {
        $host = '127.0.0.1';
        $dbname = 'wfflix';
        $user = 'root';
        $pass = 'UdonDaisuki212#';
        $charset = 'utf8mb4';

        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            return new PDO($dsn, $user, $pass, $options);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}